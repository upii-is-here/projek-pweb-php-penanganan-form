<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="tampilan.css">
</head>
<body>
  <?php
    $namaErr = $emailErr = $genderErr = $hobiErr = $komenErr = "";
    $nama    = $email = $gender = $komen = "";
    $hobi    = array();
    $formSubmitted = false;

    if($_SERVER["REQUEST_METHOD"]  == "POST"){
      if(empty($_POST["nama"])){
        $namaErr = "Name is required!";
      }
      else{
        $nama = test_input($_POST["nama"]);
        if(!preg_match("/^[a-zA-Z ]*$/",$nama)){
          $namaErr = "Only letters and white space allowed!";
        }
      }

      if(empty($_POST["email"])){
        $emailErr = "Email is required!";
      }
      else{
        $email = test_input($_POST["email"]);
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
          $emailErr = "Invalid email format!";
        }
      }

      if(empty($_POST["gender"])){
        $genderErr = "Gender is required!";
      }
      else{
        $gender = test_input($_POST["gender"]);
      }

      if(!empty($_POST["hobi"])){
        $hobi = $_POST["hobi"];
      }

      if(empty($_POST["komen"])){
        $komenErr = "";
      }
      else{
        $komen = test_input($_POST["komen"]);
      }

      if (empty($namaErr) && empty($emailErr) && empty($genderErr)) {
        $formSubmitted = true;
      }
    }

    function test_input($data){
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }  

  ?>
  <h2>Penanganan Form Menggunakan PHP</h2>
  <div class="container">
  <?php if (!$formSubmitted): ?>
    <p>
      <span class="error">
        * required field.
      </span>
    </p>
  <?php endif; ?>

  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
    Nama: <input type="text" name="nama" value="<?php echo $nama;?>">
    <?php if (empty($nama)): ?>
    <span class="error">* <?php echo $namaErr;?></span>
    <?php endif; ?>
    <br><br>
    Email: <input type="text" name="email" value="<?php echo $email;?>">
    <?php if (empty($email)): ?>
    <span class="error">* <?php echo $emailErr;?></span>
    <?php endif; ?>
    <br><br>
    Gender: 
    <input type="radio" name="gender" <?php
    if(isset($gender) && $gender == "Pria") echo "checked";?> value="Pria">Pria
    <input type="radio" name="gender" <?php
    if(isset($gender) && $gender == "Wanita") echo "checked";?> value="Wanita">Wanita
    <?php if (empty($gender)): ?>
    <span class="error">* <?php echo $genderErr;?></span>
    <?php endif; ?>
    <br><br>
    Hobi: 
    <label for="game"><input type="checkbox" id="game" name="hobi[]" value="Playing Game">Game</label>
    <label for="reading"><input type="checkbox" id="reading" name="hobi[]" value="Sport">Sport</label>
    <label for="film"><input type="checkbox" id="film" name="hobi[]" value="Watching Film">Film</label>
    <label for="film"><input type="checkbox" id="film" name="hobi[]" value="Reading Book">Reading</label>
    <br><br>
    Komentar: <br><textarea name="komen" rows="2" cols="40">
    <?php echo $komen;?>
    </textarea>
    <br><br>
    <input type="submit" name="submit" value="Sumbit">
  </form>
</div>
  <?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($namaErr) && empty($emailErr) && empty($genderErr)) {
    echo "<h2>Hasil Input:</h2>";
    echo "<p>Nama: $nama</p>";
    echo "<p>Email: $email</p>";
    echo "<p>Gender: $gender</p>";
    if (!empty($hobi)) {
        echo "<p>Hobi: " . implode(", ", $hobi) . "</p>";
    }
    echo "<p>Komentar: $komen</p>";
}
?>

</body>
</html>